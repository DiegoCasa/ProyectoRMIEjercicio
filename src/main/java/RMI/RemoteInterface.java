/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RMI;

import java.rmi.Remote;

/**
 *
 * @author diegocasa
 */
public interface RemoteInterface extends Remote{
    public String nomUser(String correo)throws Exception;
    public String domain(String correo)throws Exception;
    public String lengthText(String correo)throws Exception;
    public String countV(String correo)throws Exception;
    public String countC(String correo) throws Exception;
}
