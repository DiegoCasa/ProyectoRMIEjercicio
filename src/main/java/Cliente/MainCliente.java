/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

import RMI.RemoteInterface;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import javax.swing.JOptionPane;
/**
 *
 * @author diegocasa
 */
public class MainCliente {
    
    public static void main(String[] args) {
        try{
            String valorCorreo = JOptionPane.showInputDialog("Ingrese un Correo");
            String correo = valorCorreo;
            Registry miRegistro = LocateRegistry.getRegistry("127.0.0.1", 3456);
            RemoteInterface s = (RemoteInterface) miRegistro.lookup("Correo");
            JOptionPane.showMessageDialog(null,"El nombre de usuario: "+s.nomUser(correo));
            JOptionPane.showMessageDialog(null,"El dominio: "+s.domain(correo));
            JOptionPane.showMessageDialog(null,"El largo de la cadena de texto: "+s.lengthText(correo));
            JOptionPane.showMessageDialog(null,"La cantidad de vocales: "+s.countV(correo));
            JOptionPane.showMessageDialog(null,"RLa cantidad de consonantes: "+s.countC(correo));
        }catch (Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
    
}
