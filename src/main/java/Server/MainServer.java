/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 *
 * @author diegocasa
 */
public class MainServer {
    public static void main(String[] args) {
        try{
            Registry miRegistry = LocateRegistry.createRegistry(3456);
            miRegistry.rebind("Correo", new ServerImplements());
            System.out.println("Server On Correo");
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
