/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import RMI.RemoteInterface;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author diegocasa
 */
public class ServerImplements extends UnicastRemoteObject implements RemoteInterface{
    public ServerImplements() throws Exception{
        super();    
    }
    @Override
    public String nomUser(String correo) {
        String string = correo;
        String[] parts = string.split("@");
        String part1 = parts[0].toString();
        return part1; 
    }

    @Override
    public String domain(String correo) {
        String string = correo;
        String[] parts = string.split("@");
        String part2 = parts[1].toString();
        return part2; 
    }

    @Override
    public String lengthText(String correo) {
        return String.valueOf(correo.length());
    }

    @Override
    public String countV(String correo) {
        int contador = 0;
        for(int x=0;x<correo.length();x++) {
            if ((correo.charAt(x)=='a') || (correo.charAt(x)=='e') || (correo.charAt(x)=='i') || (correo.charAt(x)=='o') || (correo.charAt(x)=='u')){ 
               contador++;
            }
        }
        return String.valueOf(contador);
    }

    @Override
    public String countC(String correo) {
        int numConsonants = correo.replaceAll("(?i)[^bcdfghjklmnpqrstvwxyz]", "").length();
        return String.valueOf(numConsonants);
    }
    
}
